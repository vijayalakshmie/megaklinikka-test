# Megaklinikka coding test

## Part 1: word to a rectangular frame

### Setup instructions

1. Open command prompt from inside the folder <b>wordtorectangularframe</b>
2. Run command ```npm install```
3. After installation of node_modules, run command ```npm start```
4. Open the browser with address localhost:4200

## Part 2: create a webservice endpoint using [SparkJava](http://sparkjava.com/)

1. Open IntelliJ, and open the project <b>sparkjavawebservice</b>
2. Run App.main() under the src folder
3. Open Postman, and make a POST request localhost:4567/hash with an input for example

```
{"text":"finland"}
```

4. Output:

```
{
    "hash": "243801106c4d7b8f43fb87ea881d0e8e079435d199132d411bc612ffb56f9e6e"
}
```