import { element } from 'protractor';
import { Component, Input } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  textValue = 'Hello this is Finland';
  rectangularFrame = '';


  printInFrame() {
    const words = this.textValue.split(' ');

    const newWords = [];
    const longest = words.reduce(function (a, b) { return a.length > b.length ? a : b; }).length;

    newWords.push('*'.repeat(longest + 4));
    words.forEach((element: string) => {
      const elementLength = element.length;
      const numOfSpaces = (longest + 1) - elementLength;

      newWords.push('* ' + element + ' '.repeat(numOfSpaces) + '*');
    });
    newWords.push('*'.repeat(longest + 4));
    this.rectangularFrame = newWords.join('\n');
  }
}
